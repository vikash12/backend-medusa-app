package co.innoventes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MedusaBackendApp {

	public static void main(String[] args) {
		SpringApplication.run(MedusaBackendApp.class, args);
	}

}
