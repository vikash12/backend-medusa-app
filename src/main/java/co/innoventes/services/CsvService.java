package co.innoventes.services;

import co.innoventes.exception.S3UploadException;
import com.amazonaws.services.s3.AmazonS3;
import com.amazonaws.services.s3.model.ObjectMetadata;
import com.amazonaws.services.s3.model.PutObjectRequest;
import com.amazonaws.services.s3.model.S3Object;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.io.InputStream;

@Service
public class CsvService {


    @Autowired
    private AmazonS3 amazonS3; // Autowired Amazon S3 client

    @Value("${aws.bucket.name}") // Define your S3 bucket name in application.yml
    private String s3BucketName;

    public void uploadCsv(InputStream fileInputStream, String fileName) {
        try {
            // Create an S3 ObjectMetadata
            ObjectMetadata metadata = new ObjectMetadata();
            metadata.setContentType("text/csv"); // Set content type
            // Upload the CSV file to S3
            amazonS3.putObject(new PutObjectRequest(s3BucketName, "data_platform/input/" + fileName, fileInputStream , metadata));
        } catch (Exception e) {
            throw new S3UploadException("Failed to upload CSV file to S3: " + e.getMessage());
        }
    }

    public InputStream downloadFileFromS3(String bucketName, String key) throws IOException {
        // Download the file from S3
        S3Object s3Object = amazonS3.getObject(bucketName, "data_platform/output/" + key);
        return s3Object.getObjectContent();
    }
}
