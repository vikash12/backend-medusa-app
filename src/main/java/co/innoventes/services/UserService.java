package co.innoventes.services;

import co.innoventes.exception.CommonException;
import co.innoventes.repository.UserRepository;
import co.innoventes.request.ChangePasswordRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;



@Service
@RequiredArgsConstructor
public class UserService {

    private final PasswordEncoder passwordEncoder;
    private final UserRepository userRepository;
    private final JwtService jwtService;
    public void changePassword(ChangePasswordRequest request , String token) {

        if (token == null || !token.startsWith("Bearer ")) {
            throw new CommonException("Try after some time");
        }

        final String refreshToken = token.substring(7);
        final String userEmail = jwtService.extractUsername(refreshToken);

        if (userEmail != null) {
            var user = this.userRepository.findByEmail(userEmail)
                    .orElseThrow(() -> new CommonException("Try after some time"));

            if(!request.getCurrentPassword().equals(request.getConfirmationPassword())) {
                throw new CommonException("Current and confirmation password are different");
            }

            if(!user.getPassword().equals(passwordEncoder.encode(request.getCurrentPassword()))) {
                throw new CommonException("Password does not match");
            }
            System.out.println(user);

            user.setPassword(passwordEncoder.encode(request.getNewPassword()));
            userRepository.save(user);

        }
    }
}
