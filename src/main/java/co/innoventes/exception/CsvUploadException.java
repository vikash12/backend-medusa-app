package co.innoventes.exception;

public class CsvUploadException extends RuntimeException {
    public CsvUploadException(String message) {
        super(message);
    }
}
