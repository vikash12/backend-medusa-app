package co.innoventes.controller;

import co.innoventes.exception.CsvUploadException;
import co.innoventes.exception.S3UploadException;
import co.innoventes.services.CsvService;
import co.innoventes.utility.Constants;
import com.amazonaws.services.s3.AmazonS3;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.io.InputStream;

@RestController()
@RequestMapping("csv")
public class CsvController {

    @Autowired
    private AmazonS3 amazonS3;

    @Autowired
    private CsvService csvService;

    @Value("${aws.bucket.name}") // Define your S3 bucket name in application.yml
    private String s3BucketName;

    @PostMapping("/upload")
    public String uploadCsv(@RequestParam("csv-file") MultipartFile file) {
        System.out.println("api is calling");
        try {
            String fileName = file.getOriginalFilename();
            if (fileName != null && fileName.toLowerCase().endsWith(".csv")) {
                csvService.uploadCsv(file.getInputStream(), Constants.INPUT_CSV_FILE);
                return "CSV uploaded successfully!";
            } else {
                throw new CsvUploadException("File must have a '.csv' extension.");
            }

        } catch (CsvUploadException ex) {
            throw ex; // Let the global exception handler handle this exception
        } catch (S3UploadException ex) {
            throw ex; // Let the global exception handler handle this exception
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @GetMapping("/download")
    public ResponseEntity<InputStreamResource> downloadFromS3() {
        try {
            // Delegate the download to the service

            InputStream inputStream = csvService.downloadFileFromS3(s3BucketName, Constants.OUTPUT_CSV_FILE);

            // Set up HTTP headers
            HttpHeaders headers = new HttpHeaders();
            headers.setContentDispositionFormData("attachment", Constants.OUTPUT_CSV_FILE);
            headers.setContentType(MediaType.APPLICATION_OCTET_STREAM);

            // Create an InputStreamResource to wrap the file content
            InputStreamResource resource = new InputStreamResource(inputStream);

            // Return the file as a response
            return ResponseEntity.ok()
                    .headers(headers)
                    .body(resource);

        } catch (IOException e) {
            // Handle exceptions appropriately
            return ResponseEntity.badRequest().body(null);
        }
    }

    @GetMapping("/hello")
    public String testHello() {
        return "Hello, World!";
    }

}
