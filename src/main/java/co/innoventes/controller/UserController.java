package co.innoventes.controller;

import co.innoventes.request.ChangePasswordRequest;
import co.innoventes.services.UserService;
import jakarta.servlet.http.HttpServletRequest;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;

@RestController
@RequestMapping("users")
@RequiredArgsConstructor
public class UserController {

    private final UserService service;

    @PatchMapping("/change-password")
    public ResponseEntity<?> changePassword(HttpServletRequest request ,
                                            @RequestBody ChangePasswordRequest passwordRequest
    ) {
        String token = request.getHeader("Authorization");
        service.changePassword(passwordRequest , token);
        return ResponseEntity.status(HttpStatus.OK).body("password change successfully");
    }
}
