package co.innoventes.utility;

public class Constants {

    public static final String INPUT_CSV_FILE = "input-testing.csv";
    public static final String OUTPUT_CSV_FILE = "feature_data.csv";
}
